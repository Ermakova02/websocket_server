package ru.nsu.ccfit.ermakova.wschat.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;
import java.io.StringReader;
import java.util.UUID;

public class MessageDecoder implements Decoder.Text<Message> {

    @Override
    public void init(final EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }

    @Override
    public Message decode(final String textMessage) throws DecodeException {
        JsonObject jsonObject = Json.createReader(new StringReader(textMessage)).readObject();
        UUID uuid;
        String struuid = jsonObject.getString("sender");
        if (struuid.equals("null")) uuid = null;
        else uuid = UUID.fromString(struuid);
        Message message = new Message(jsonObject.getBoolean("servermessage"),
                jsonObject.getString("content"),
                uuid,
                jsonObject.getString("name"),
                Long.parseLong(jsonObject.getString("time")));
        return message;
    }

    @Override
    public boolean willDecode(final String s) {
        return true;
    }

}

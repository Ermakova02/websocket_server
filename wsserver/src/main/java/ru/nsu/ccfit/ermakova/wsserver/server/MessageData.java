package ru.nsu.ccfit.ermakova.wsserver.server;

import java.util.UUID;

public class MessageData {
    public int id;
    public boolean serverMessage;
    public int userID;
    public String message;
    public long time;

    MessageData(int id, boolean serverMessage, int userID, String message) {
        this.id = id;
        this.serverMessage = serverMessage;
        this.userID = userID;
        this.message = message;
        this.time = System.currentTimeMillis();
    }
}

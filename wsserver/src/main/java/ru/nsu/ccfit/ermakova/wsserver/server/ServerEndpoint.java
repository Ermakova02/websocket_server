package ru.nsu.ccfit.ermakova.wsserver.server;

import ru.nsu.ccfit.ermakova.wschat.model.Message;
import ru.nsu.ccfit.ermakova.wschat.model.MessageDecoder;
import ru.nsu.ccfit.ermakova.wschat.model.MessageEncoder;

//import static java.lang.String.format;

import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

@javax.websocket.server.ServerEndpoint(value = "/chat", encoders = MessageEncoder.class, decoders = MessageDecoder.class)
public class ServerEndpoint {

    static Set<Session> peers = Collections.synchronizedSet(new HashSet<Session>());

    @OnOpen
    public void onOpen(Session session) {
        peers.add(session);
    }

    @OnMessage
    public void onMessage(Message message, Session session) throws IOException, EncodeException {
        message.setSender(null);
        message.setTime(System.currentTimeMillis());
        for (Session peer : peers) {
            if (!message.isServerMessage() | !session.getId().equals(peer.getId())) {
                peer.getBasicRemote().sendObject(message);
            }
        }
    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        peers.remove(session);
    }
}

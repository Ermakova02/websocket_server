package ru.nsu.ccfit.ermakova.wschat.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Message {
    private boolean serverMessage;
    private String content;
    private UUID sender;
    private String senderName;
    private long time;

    public Message() {
        serverMessage = false;
        content = null;
        sender = null;
    }

    public Message(boolean serverMessage, String content, UUID sender, String senderName, long time) {
        this.serverMessage = serverMessage;
        this.content = content;
        this.sender = sender;
        this.senderName = senderName;
        this.time = time;
    }

    public boolean isServerMessage() { return serverMessage; }
    public void setServerMessage(boolean serverMessage) { this.serverMessage = serverMessage; }
    public String getContent() { return content; }
    public void setContent(String content) { this.content = content; }
    public UUID getSender() { return sender; }
    public void setSender(UUID sender) { this.sender = sender; }
    public String getSenderName() { return senderName; }
    public void setSenderName(String senderName) { this.senderName = senderName; }
    public long getTime() { return time; }
    public void setTime(long time) { this.time = time; }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        Date date = new Date(time);
        DateFormat formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sb.append("[" + formattedDate.format(date) + "]\n");
        if (!serverMessage) sb.append("\"" + senderName + "\": ");
        sb.append(content + "\n");
        return sb.toString();
    }
}

package ru.nsu.ccfit.ermakova.wsserver.server;

import java.util.TimerTask;

public class ClientsUpdater extends TimerTask {
    private ServerProcessor srvProcessor = null;

    void setServer(ServerProcessor srv) { srvProcessor = srv; }
    @Override
    public void run() {
        srvProcessor.updateClients();
    }
}

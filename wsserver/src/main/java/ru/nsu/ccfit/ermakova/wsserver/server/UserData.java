package ru.nsu.ccfit.ermakova.wsserver.server;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class UserData {
    public int id;
    public String username;
    public UUID token;
    public Boolean online;
    public long time;

    UserData(int id, String username) {
        this.id = id;
        this.username = username;
        this.token = UUID.randomUUID();
        online = false;
        setUpdatedTime();
    }

    public void setUpdatedTime() {time = System.currentTimeMillis(); }

    public String toString() {
        Date date = new Date(time);
        DateFormat formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        sb.append(id);
        sb.append(", ");
        sb.append(username);
        sb.append(", ");
        sb.append(token.toString());
        sb.append(", ");
        sb.append(online);
        sb.append(", ");
        sb.append(formattedDate.format(date));
        sb.append(" }");
        return sb.toString();
    }

    public boolean equals(Object obj) {
        return ((obj instanceof UserData) && this.id == ((UserData) obj).id);
    }

}
